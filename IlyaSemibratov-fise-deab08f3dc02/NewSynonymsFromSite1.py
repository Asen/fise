# coding: utf-8
from bs4 import BeautifulSoup
import requests
import sys
import re

BASE_URL = 'http://edbi.ru/sinonim/'

def GetPage(url):
	Response = requests.post(url)
	#print (Response.text)
	#print (BASE_URL, Response)
	return Response.text


def Parse(html):
	soup = BeautifulSoup(html, 'html.parser')
	#table = soup.find_all('ol')
	#print (table)
	table = soup.find('td', valign='top')
	try:
		words = table.find_all('a')
	except AttributeError:
		print (' 	  На сайте ' + 
			   BASE_URL[:-9] +
			   ' синонимы не найдены')
		return []
	#print (words)
	synonyms = []

	counter = 0
	while counter < len(words):
		test = str(words[counter])
		synonyms.append(''.join(re.findall(r'[а-я]', test[test.find('>')+1:len(test)-4])))
		counter += 1
	print (' 	  done')
	return synonyms


	
def main(Word):
	print (' 	 Поиск синонимов на сайте: ' + BASE_URL[:-9])
	SynonymsDict = {}

	Url = BASE_URL + Word#list-s'#'#list-s' - чтобы на одной странице сразу все слова был
	Html =  GetPage(Url)
	#SynonymsDict[Word] = Parse(Html)
	#print (SynonymsDict)
	return (Parse(Html))


if __name__ == "__main__": 
	Word = sys.argv[1]
	print (main(Word))