# -*- coding: utf8 -*-
"""
Убирает синонимы из файла Слова.txt
и добавляет их в БД тэгов\синонимов
"""
import requests
from bs4 import BeautifulSoup
import sqlite3 as lite
import re
import json
import pymorphy2

KEY_API = 'dict.1.1.20170928T063748Z.57143ef7a8afe8de.f3e0a314ad794400f3f9fd7f69156b5aa8222dfd'
BASE_URL = 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key='+KEY_API+'&lang=ru-ru&text='


def GetSynonymsDict(Word, param):
	morph  = pymorphy2.MorphAnalyzer()

	#Payload = {'word': Word}
	Response = requests.post(BASE_URL+Word)
	print (Response)

	synonyms = {}
	synonyms.clear()
	synonyms['Synonyms'] = []
	synonyms[param] = Word

	ResponseTXT = str(Response.text)
	result = re.split(r'[^а-я ]', ResponseTXT)
	result = [v for v in result if v]

	if len(result) == 0:
		synonyms['Synonyms'] = ['NotFound']
		return synonyms
	else:
		synonyms['Synonyms'] = result

	counter = 0
	while counter < len(synonyms['Synonyms']):
		ParticleWord = morph.parse(synonyms['Synonyms'][counter])[0].tag.POS
		if synonyms['Synonyms'][counter].count(' ') > 0 or (ParticleWord != 'NOUN' and ParticleWord != 'ADJF'):
			synonyms['Synonyms'].remove(synonyms['Synonyms'][counter])
			synonyms['Synonyms'].insert(counter, '')

		counter += 1

	synonyms['Synonyms'] = [x for x in synonyms['Synonyms'] if x != '']
	print (synonyms['Synonyms'])
	return synonyms


def GetSynonymsDictionaryOnline(Tags, param, number):
	counter = 0
	SynonymsDict = {}
	while counter < len(Tags):
		Word = Tags[counter]#слово, к которому необходимо найти синонимы
		print (' 	Searching synonyms for word #%d - %s' %(number, 
															Word))

		SynonymsDict[number] = GetSynonymsDict(Word, param)

		counter += 1
		print ('			Searching done')
	return SynonymsDict


def SaveSynonymsInFile(Dictionary):
	with open('SynonymsTags1.json', 'w', encoding='utf-8') as file:
		json.dump(Dictionary, 
				  file, 
				  sort_keys=True, 
				  indent=4, 
				  ensure_ascii=False)


def SaveSynonymsInDB(SynonymsDict):
	Counter = 1
	Config_Parameters_DB = lite.connect('Synonyms1.db')
	while Counter <= len(SynonymsDict):
		CounterLst = 0
		while CounterLst < len(SynonymsDict[Counter]['Synonyms']):
			WordLst = SynonymsDict[Counter]['Synonyms'][CounterLst]
			db_action = Config_Parameters_DB.cursor()
			db_action.execute("INSERT INTO TagsSynonyms VALUES(?, ?)", 
						   	  (SynonymsDict[Counter]['TagName'], WordLst))
			CounterLst += 1
		Counter += 1
	Config_Parameters_DB.commit()
	db_action.close()



def GetErrorDict(Number, SynW, Word):
	SynWord = {}
	SynWordMain = {}
	Shit = {}

	ErList = [SynW+' - синонимичный тэг']
	SynWord['TagName'] = Word
	SynWord['Synonyms'] = ErList
	SynWordMain[Number] = SynWord 
	Shit[Number] = SynWordMain
	return Shit[Number]


def start():
	f = open('Слова.txt', 'r')
	Tags = f.readlines()
	Tags = list(set(Tags))

	Pass = 0

	ValidWords = []
	WordsDict = {}

	ValidWords.append(Tags[0].rstrip())
	WordsDict.update(GetSynonymsDictionaryOnline(ValidWords, 
							                	 'TagName', 
							                	 1))

	i = 1
	while i < len(Tags):
		Word = (Tags[i].rstrip())
		countersynonyms = 1

		while countersynonyms <= len(WordsDict):
			#print (WordsDict)
			if WordsDict[countersynonyms]['Synonyms'].count(Word) < 1:
				countersynonyms += 1
			else:
				print ('Error - синонимичный тэг!!      :' + str(Word))
				WordsDict.update(GetErrorDict(i+1, 
											  WordsDict[countersynonyms]['TagName'], 
											  Word))
				Pass = 1
				countersynonyms = 110934231
		if Pass != 1:
			WordL = []
			WordL.append(Word)
			ValidWords.append(Word)
			WordsDict.update(GetSynonymsDictionaryOnline(WordL, 
														 'TagName', 
														 i+1))
		else:
			Pass = 0
		#print (json.dumps(WordsDict, sort_keys=True, indent=4, ensure_ascii=False))
		i += 1
	#print (WordsDict)
	SaveSynonymsInFile(WordsDict)
	SaveSynonymsInDB(WordsDict)


if __name__ == "__main__": 
	start()