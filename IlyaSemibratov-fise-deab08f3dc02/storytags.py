"""
Пока не используется
"""
import pymorphy2
from bs4 import BeautifulSoup
import re
import requests
import json
import sqlite3 as lite 


#####Constants:#####
#Description of films:
Films = {'Film1': {'Name': 'дюнкерк', 
				   'StoryTags': ['война', 
				   				 'противостояние', 
				   				 'хаос']},
		 'Film2': {'Name': 'зодиак', 
		 		   'StoryTags': ['убийство', 
		 		   				 'расследование', 
		 		   				 'загадка']},
		 'Film3': {'Name': 'интерстеллар', 
		 		   'StoryTags': ['космос', 
		 		   				 'наука', 
		 		   				 'драма']},
		 'Film4': {'Name': 'апокалипсис', 
		 		   'StoryTags': ['месть', 
		 		   				 'порабощение', 
		 		   				 'противостояние']},
		 'Film5': {'Name': 'молчание', 
		 		   'StoryTags': ['драма', 
		 		   				 'судьба', 
		 		   				 'угнетение']}}
#URL for parsing synonyms:
BASE_URL = 'http://synonymonline.ru/'
TAGS_DB_NAME = 'TagsSynonyms'
PARAMETERS_DB_NAME = 'ParametersSynonyms'


def StartSearching():
	print ('СОЗДАНИЕ СПИСКА ТЕГОВ:')
	UsedTags = GetAllUsedTags(Films)

	print ('СОЗДАНИЕ СПИСКА СИНОНИМОВ К КАЖДОМУ ТЕГУ:')
	#SynonymsForEachTagDict = GetSynonymsDictionaryOnline(UsedTags, 'TagName')
	SynonymsForEachTagDict = GetSynonymsDictionaryOfline(UsedTags, 
														 'TagName', 
														 TAGS_DB_NAME)
	#SaveSynonymsInDB(SynonymsForEachTagDict)
	#SaveSynonymsInFile(SynonymsForEachTagDict)

	#Ввод тегов для поиска фильма(через пробел):
	print ('ПОИСК ФИЛЬМА:')
	FilmParameters = 'бежать предназначению'
	FilmParametersList = prettifyFilmParameters(FilmParameters.split())
	print ('	Введенные параметры: %s' %', '.join(FilmParametersList))
	#SynonymsForFilmParametersDict = GetSynonymsDictionaryOnline(FilmParametersList, 'ParameterName')
	SynonymsForFilmParametersDict = GetSynonymsDictionaryOfline(FilmParametersList, 
																'ParameterName', 
																PARAMETERS_DB_NAME)
	print ('	Выбор тегов с учетом введенных параметров')
	TagsForFilm = GetTagsForFilm(SynonymsForFilmParametersDict, 
								 SynonymsForEachTagDict)
	print (' 	Выбор фильма, который содержит необходимые теги')
	FilmsSolution = GetFilms(TagsForFilm)
	print ('			РЕКОМЕНДУЕМЫЕ ФИЛЬМЫ: %s' %', '.join(FilmsSolution))


def GetAllUsedTags(Films):
	#####Получение списка всех тегов#####
	Counter = 1
	Tags = []
	while Counter <= len(Films):
		CounterList = 0
		while CounterList < len(Films['Film'+str(Counter)]['StoryTags']):
			if Tags.count(Films['Film'+str(Counter)]['StoryTags'][CounterList]) == 0:
				Tags.append(Films['Film'+str(Counter)]['StoryTags'][CounterList])
			CounterList += 1
		Counter += 1
	return Tags


def GetPage(Word):
	Response = requests.get(BASE_URL+Word[0].upper()+'/'+Word)
	return Response.text


def Parse(html, word, param):
	soup = BeautifulSoup(html, 'html.parser')
	table = soup.find(class_='synonyms-list')
	words = table.find_all('span')
	
	synonyms = {}
	synonyms[param] = word
	synonyms['Synonyms'] = []

	counter = 0
	while counter < len(words):
		test = str(words[counter])
		synonyms['Synonyms'].append(''.join(re.findall('[^span<>/]', 
								    test)))
		print ('Synonim - ', 
			   synonyms['Synonyms']) #посмотреть, что за синоним и удалить тот, где больше 1го слова
		counter += 1
	print ('			Parsing done') 
	return synonyms


def GetSynonymsDictionaryOnline(Tags, param):
	counter = 0
	SynonymsDict = {}
	while counter < len(Tags):
		Word = Tags[counter]#слово, к которому необходимо найти синонимы
		print (' 	Parsing synonyms for word #%d - %s' %(counter+1, 
														  Word))

		Html = GetPage(Word)
		SynonymsDict[counter+1] = Parse(Html, Word, param)

		counter += 1
	return SynonymsDict


def GetSynonymsDictionaryOfline(TagsOrParameters, 
								param, DB_NAME):
	if DB_NAME == TAGS_DB_NAME:
		ColumnName = 'Tag'
	else:
		ColumnName = 'Parameter'
	counter = 0
	SynonymsDict = {}
	while counter < len(TagsOrParameters):
		Word = TagsOrParameters[counter]#слово, к которому необходимо найти синонимы
		print (' 	Searching synonyms in DB for word #%d - %s' %(counter+1, Word))

		Config_Parameters_DB = lite.connect('Synonyms.db')
		db_action = Config_Parameters_DB.cursor()
		sql = ('SELECT Synonyms FROM ' + 
			   DB_NAME + ' WHERE ' + 
			   ColumnName +'=?')
		db_action.execute(sql, [Word])
		Synonyms = str(db_action.fetchone())[2:-3].split(', ')
		if ''.join(Synonyms) == '':
			print('		Word in DB not found. Try to parse word')
			SynonymsDict = GetSynonymsDictionaryOnline(TagsOrParameters, param)
			Config_Parameters_DB.commit()
			db_action.close()
			return SynonymsDict	
		else:
			Config_Parameters_DB.commit()
			db_action.close()

			SynonymDict = {}
			SynonymDict[param] = Word
			SynonymDict['Synonyms'] = Synonyms

			SynonymsDict[counter+1] = SynonymDict

			print ('			Searching done')
		counter += 1
	return SynonymsDict	


def SaveSynonymsInDB(SynonymsDict):
	Counter = 1
	while Counter <= len(SynonymsDict):
		Config_Parameters_DB = lite.connect('Synonyms.db')
		db_action = Config_Parameters_DB.cursor()
		db_action.execute('SELECT Synonyms FROM TagsSynonyms Where Tag=?', 
						  [SynonymsDict[Counter]['TagName']])
		if db_action.fetchone() == None:
			db_action.execute("INSERT INTO TagsSynonyms VALUES(?, ?)", 
							  (SynonymsDict[Counter]['TagName'], 
							  ', '.join(SynonymsDict[Counter]['Synonyms'])))
			Config_Parameters_DB.commit()
		Counter += 1
	db_action.close()


def SaveSynonymsInFile(Dictionary):
	with open('SynonymsTags.json', 'w', encoding='utf-8') as file:
		json.dump(Dictionary, file, sort_keys=True, indent=4)


def prettifyFilmParameters(parameterslist):
	counter = 0
	morph  = pymorphy2.MorphAnalyzer()
	while counter < len(parameterslist):

		parameter = parameterslist[counter].lower()
		#parameter = SpellChecker(parameter)#Later

		parameterNormalForm = morph.parse(parameter)[0].normal_form
		print ('Параметр ТЕСТ')
		if morph.parse(parameter)[0].tag.POS != 'NOUN':
			print ('Это ne существительное')
			print (morph.parse(parameter)[0].inflect({'sing', 'nomn'}).word)

		print (parameterNormalForm)

		parameterslist[counter] = parameterNormalForm

		counter += 1
	return parameterslist


def SpellChecker(word1):
	pass#Later


def GetTagsForFilm(ParametersDict, TagDict):
	FilmTags = []
	CounterA = 0
	Flag = 0
	while CounterA < len(TagDict):
		Tag = TagDict[CounterA+1]['TagName']
		CounterB = 0
		while CounterB < len(ParametersDict):
			if Flag == 1:
				Flag = 0
				break
			if (ParametersDict[CounterB+1]['ParameterName'] == Tag or 
				(ParametersDict[CounterB+1]['ParameterName'] in 
				 TagDict[CounterA+1]['Synonyms']) == True):
				FilmTags.append(Tag)

				Config_Parameters_DB = lite.connect('Synonyms.db')
				db_action = Config_Parameters_DB.cursor()
				db_action.execute('SELECT Synonyms FROM ParametersSynonyms Where Parameter=?', 
								  [ParametersDict[CounterB+1]['ParameterName']])
				if db_action.fetchone() == None:
					db_action.execute("INSERT INTO ParametersSynonyms VALUES(?, ?)", 
						              (ParametersDict[CounterB+1]['ParameterName'], 
							          ', '.join(ParametersDict[CounterB+1]['Synonyms'])))
					Config_Parameters_DB.commit()
				db_action.close()

				break
			CounterC = 0
			while CounterC < len(ParametersDict[CounterB+1]['Synonyms']):
				if (ParametersDict[CounterB+1]['Synonyms'][CounterC] in TagDict[CounterA+1]['Synonyms'] == True):
					FilmTags.append(Tag)

					Config_Parameters_DB = lite.connect('Synonyms.db')
					db_action = Config_Parameters_DB.cursor()
					db_action.execute('SELECT Synonyms FROM ParametersSynonyms Where Parameter=?', 
									  [ParametersDict[CounterB+1]['ParameterName']])
					if db_action.fetchone() == None:
						db_action.execute("INSERT INTO ParametersSynonyms VALUES(?, ?)", 
							              (ParametersDict[CounterB+1]['ParameterName'], 
								          ', '.join(ParametersDict[CounterB+1]['Synonyms'])))
						Config_Parameters_DB.commit()
					db_action.close()

					Flag = 1
					break
				CounterC += 1
			CounterB += 1
		CounterA += 1
	print ('			Выбранные теги - %s' % ', '.join(FilmTags))
	return FilmTags


def GetFilms(Tags):
	Solution = []
	Counter = 0
	while Counter < len(Films):
		CounterEl = 0
		while CounterEl < len(Tags):
			if (Tags[CounterEl] in Films['Film'+str(Counter+1)]['StoryTags']) == True:
				Solution.append(Films['Film'+str(Counter+1)]['Name'])
			CounterEl += 1
		Counter += 1
	return Solution


if __name__ == "__main__": 
	StartSearching()