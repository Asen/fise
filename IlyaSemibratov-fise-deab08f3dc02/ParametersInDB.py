# -*- coding: utf8 -*-
"""
Убирает синонимы из файла Слова.txt
и добавляет их в БД тэгов\синонимов
"""
import requests
from bs4 import BeautifulSoup
import sqlite3 as lite
import re
import json
import pymorphy2
import GetSynonyms


def GetSynonymsDictionaryOnline(Tags, param, number):
	counter = 0
	SynonymsDict = {}
	while counter < len(Tags):
		Word = Tags[counter]#слово, к которому необходимо найти синонимы
		print (' 	Searching synonyms for word #%d - %s' %(number, 
															Word))
		
		SynonymsDict[number] = GetSynonyms.GetSynonyms(param, Word)
		print (SynonymsDict[number])

		counter += 1
	return SynonymsDict


def SaveSynonymsInFile(Dictionary):
	with open('SynonymsTags.json', 'w', encoding='utf-8') as file:
		json.dump(Dictionary, 
				  file, 
				  sort_keys=True, 
				  indent=4, 
				  ensure_ascii=False)


def SaveSynonymsInDB(SynonymsDict):
	Counter = 1
	Config_Parameters_DB = lite.connect('Synonyms.db')
	while Counter <= len(SynonymsDict):
		CounterLst = 0
		while CounterLst < len(SynonymsDict[Counter]['Synonyms']):
			WordLst = SynonymsDict[Counter]['Synonyms'][CounterLst]
			db_action = Config_Parameters_DB.cursor()
			db_action.execute("INSERT INTO TagsSynonyms VALUES(?, ?)", 
						   	  (SynonymsDict[Counter]['TagName'], WordLst))
			CounterLst += 1
		Counter += 1
	Config_Parameters_DB.commit()
	db_action.close()



def GetErrorDict(Number, SynW, Word):
	SynWord = {}
	SynWordMain = {}
	Shit = {}

	ErList = [SynW+' - синонимичный тэг']
	SynWord['TagName'] = Word
	SynWord['Synonyms'] = ErList
	SynWordMain[Number] = SynWord 
	Shit[Number] = SynWordMain
	return Shit[Number]


def start():
	f = open('Слова.txt', 'r')
	Tags = f.readlines()
	Tags = list(set(Tags))

	Pass = 0

	ValidWords = []
	WordsDict = {}

	ValidWords.append(Tags[0].rstrip())
	WordsDict.update(GetSynonymsDictionaryOnline(ValidWords, 
							                	 'TagName', 
							                	 1))

	i = 1
	while i < len(Tags):
		Word = (Tags[i].rstrip())
		countersynonyms = 1

		while countersynonyms <= len(WordsDict):
			#print (WordsDict)
			if WordsDict[countersynonyms]['Synonyms'].count(Word) < 1:
				countersynonyms += 1
			else:
				print (' 	Error - синонимичный тэг!!      :' + str(Word))
				WordsDict.update(GetErrorDict(i+1, 
											  WordsDict[countersynonyms]['TagName'], 
											  Word))
				Pass = 1
				countersynonyms = 110934231
		if Pass != 1:
			WordL = []
			WordL.append(Word)
			ValidWords.append(Word)
			WordsDict.update(GetSynonymsDictionaryOnline(WordL, 
														 'TagName', 
														 i+1))
		else:
			Pass = 0

		i += 1
	SaveSynonymsInFile(WordsDict)
	SaveSynonymsInDB(WordsDict)


if __name__ == "__main__": 
	start()