"""
Рекомендация фильмов по введенным словам
"""
import pymorphy2
import sqlite3 as lite
import os
import json

DIRECTORY = '/media/sf_D_DRIVE/FILMPRJ/films'




def PrettyWords(WordsList):
	morph  = pymorphy2.MorphAnalyzer()
	WordsDict = {}

	WordsCounter = 0
	while WordsCounter < len(WordsList):
		Word = WordsList[WordsCounter]
		WordsList[WordsCounter].lower()
		#parameter = SpellChecker(parameter)#Later
		'''
		Определение части речи
		'''
		ParticleWord = morph.parse(Word)[0].tag.POS
		if ParticleWord == 'NOUN' or ParticleWord == 'ADJF':
			
			WordsList[WordsCounter] = morph.parse(Word)[0].normal_form
		else:
			print (Word, '- не существительное\прилагательное')
		WordsCounter += 1	
	return WordsList



def CheckWordInDB(Word):

	db_action = Config_Parameters_DB.cursor()
	db_action.execute('SELECT Tag FROM TagsSynonyms Where Synonyms=? or Tag=?', 
				 	  [Word, Word])
	
	Tag = str(db_action.fetchone())[2:-3]
	if Tag == '':
		return 'Error'
	else:
		return Tag

	db_action.close()



def SearchInClass(Tag, ClassName, ClassDump):
	ClassCounter = 0
	while ClassCounter < len(ClassDump):
		Dict = ClassDump[ClassCounter]
		
		if len(Dict['features']) != 0:
			if Dict['features'].count(Tag) != 0:
				return 'Success'

		if Dict.get('specials') != None:
			if Dict['specials'].count(Tag) != 0:
				return 'Success'

		return 'Fail'			
		
		ClassCounter += 1



def GetClass(ClassCode):
	if ClassCode == '1':
		return 'stories'
	if ClassCode == '2':
		return 'heroes'
	if ClassCode == '3':
		return 'places'
	if ClassCode == '4':
		return 'times'
	if ClassCode == '5':
		return 'atmospheres'
	if ClassCode == '6':
		return 'problems'



def ChoosingFilms(ClassCode, 
				  TagsList, 
				  NumberOfValidWords, 
				  NumberOfWords):
	
	Films = {}
	FilmsNamesList = os.listdir(DIRECTORY)
	"""
	Поиск в описаниях фильмов выбранных тэгов
	"""	
	FilmsCounter = 0
	while FilmsCounter < len(FilmsNamesList):
		with open('films/'+str(FilmsNamesList[FilmsCounter]), 
				  'r', encoding='utf-8') as file:
		
			Dump = json.load(file)
			file.close()
		FilmName = Dump['name']
		print ('     Поиск тэгов в фильме №%d - %s' %(FilmsCounter+1, FilmName))
		
		ClassName = GetClass(ClassCode)
		NumberOFTags = len(TagsList)
		NumberValidTagsForFilm = 0
		TagsCounter = 0
		while TagsCounter < NumberOFTags:
			Tag = TagsList[TagsCounter]
			print ('      Поиск тэга %s ' %(Tag.upper()))
			SearchStatus = SearchInClass(Tag, 
										 ClassName, 
										 Dump[ClassName])
			print ('      ', SearchStatus)
			if SearchStatus == 'Success':
				NumberValidTagsForFilm += 1
			TagsCounter += 1
		if NumberValidTagsForFilm != 0:
			List = {}
			List[FilmName] = round(NumberValidTagsForFilm/NumberOfWords*100, 1)
			Films.update(List)
		FilmsCounter += 1
	"""
	Сортировка словаря с фильмами по значению
	"""
	l = lambda x: x[1]
	Films = sorted(Films.items(), key=l, reverse=True)
	return Films



def GetPrettyFilmsStr(FilmsList):
	FilmsCounter = 0
	while FilmsCounter < len(FilmsList):
		Film = FilmsList[FilmsCounter][0]
		Value = str(FilmsList[FilmsCounter][1]) + '%'
		PString = Film + ': ' + Value
		print (' ', str(FilmsCounter+1)+')', PString)
		FilmsCounter += 1



def GetFilmsRecomendation(ClassCode, 
			     		  WordsList):
	FilmsList = {}
	ValidWordsList = []
	TagsList = []

	print ('  Приведение слов к нормальному виду')
	PWordsList = PrettyWords(WordsList)
	#PWordsList = WordsList#for test without pymorphy2
	"""
	Поиск тэгов для введенных слов
	"""
	WordsCounter = 0
	while WordsCounter < len(PWordsList):
		Word = PWordsList[WordsCounter]
		print ('   Поиск тэга к слову %s' %(Word.upper()))
		TagForWord = CheckWordInDB(Word)
		if TagForWord != 'Error':
			print ('    Найденный тэг - %s' %(TagForWord.upper()))
			ValidWordsList.append(Word)
			TagsList.append(TagForWord)
		else:
			print ('     К слову %s тэг не найден' %(Word.upper()))
			print ('      Добавление тега в таблицу NewWords')
			db_action = Config_Parameters_DB.cursor()
			db_action.execute('SELECT Word FROM NewWords Where Word=?', 
						      [Word])
			WordINDB = str(db_action.fetchone())[2:-3]
			if len(WordINDB) == 0:
				db_action.execute("INSERT INTO NewWords VALUES(?, ?)", 
							   	  [Word, GetClass(ClassCode)])
			db_action.close()
			print ('      done')

		WordsCounter += 1
	"""
	Если тэги найдены, то список передается функции ChoosingFilms
	для выбора рекомендационных фильмов 
	"""
	if len(TagsList) != 0:
		FilmsList = ChoosingFilms(ClassCode, 
								  TagsList, 
								  len(ValidWordsList), 
								  len(WordsList))
	"""
	Вывод рекомендуемых фильмов, если они были найдены
	"""
	if len(FilmsList) != 0:
		print ('Рекомендуемые фильмы к словам %s:'\
			   %(', '.join(ValidWordsList).upper()))
		"""
		Обработка словаря с рекомендуемыми фильмами для печати
		"""
		GetPrettyFilmsStr(FilmsList)
	else:
		print ('Фильмы не найдены')



def test():
	print ('''По какой сфере искать фильм?
		   1 - stories
		   2 - heroes
		   3 - places
		   4 - times
		   5 - atmospheres
		   6 - problems''')
	ClassCode = input('Ввод: ')
	while  ClassCode != '1' and\
		   ClassCode != '2' and\
	   	   ClassCode != '3' and\
	       ClassCode != '4' and\
	       ClassCode != '5' and\
	       ClassCode != '6':

		print ('Неправильный ввод')
		ClassCode = input('Ввод: ')

	print (' Слова для поиска фильма по сфере %s (ввод слов через пробел)'\
		                                   %(GetClass(ClassCode).upper()))
	WordsList = input(' Ввод: ').split()
	GetFilmsRecomendation(ClassCode, WordsList)



if __name__ == "__main__": 
	Config_Parameters_DB = lite.connect('Synonyms.db')
	test()
	Config_Parameters_DB.commit()