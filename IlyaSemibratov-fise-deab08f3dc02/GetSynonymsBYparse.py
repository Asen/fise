# coding: utf-8
from bs4 import BeautifulSoup
import urllib

BASE_URL = 'http://sinonim.org/s/'

def GetPage(url):
	Response = urllib.urlopen(url)
	return Response.read()


def Parse(html):
	soup = BeautifulSoup(html, 'html.parser')
	table = soup.find('table')
	words = table.find_all('a')
	synonyms = []

	counter = 0
	while counter < len(words):
		test = str(words[counter])
		synonyms.append(test[test.find('>')+1:len(test)-4])
		counter += 1
	return synonyms
	#ex = soup.find('body')
	#ex = ex.find(class_='outtable')
	
def main():
	SynonymsDict = {}
	Word = 'война'#слово, к которому необходимо найти синонимы
	Url = BASE_URL + Word# + '#list-s'#'#list-s' - чтобы на одной странице сразу все слова были
	Html =  GetPage(Url)
	SynonymsDict[Word] = Parse(Html)
	return SynonymsDict


if __name__ == "__main__": 
	print (main())