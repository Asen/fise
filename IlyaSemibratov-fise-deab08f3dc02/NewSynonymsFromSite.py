# coding: utf-8
from bs4 import BeautifulSoup
import requests
import sys
import re

BASE_URL = 'http://abcsynonym.ru/'

def GetPage(url):
	Response = requests.post(url)
	#print (Response.text)
	#print (BASE_URL, Response)
	return Response.text


def Parse(html):
	soup = BeautifulSoup(html, 'html.parser')
	#table = soup.find_all('ol')
	#print (table)
	words = soup.find_all('li')[3:]
	#print (words)
	synonyms = []

	counter = 0
	while counter < len(words):
		test = str(words[counter])
		synonyms.append(''.join(re.findall(r'[а-я]', test[test.find('>')+1:len(test)-5])))
		counter += 1
	return synonyms
	#ex = soup.find('body')
	#ex = ex.find(class_='outtable')
	
def main(Word):
	print (' 	 Поиск синонимов на сайте: ' + BASE_URL[:-1])
	SynonymsDict = {}

	Url = BASE_URL + Word[0].upper() + '/' + Word.lower() + '.html'# + '#list-s'#'#list-s' - чтобы на одной странице сразу все слова был
	Html =  GetPage(Url)
	#SynonymsDict[Word] = Parse(Html)
	#print (SynonymsDict)
	print (' 	  done')
	return (Parse(Html))


if __name__ == "__main__": 
	Word = sys.argv[1]
	print (main(Word))