import requests
from bs4 import BeautifulSoup
import re
import pymorphy2
import sys
import NewSynonymsFromSite, NewSynonymsFromSite1

BASE_URL = 'http://synonymonline.ru/assets/json/synonyms.json'
KEY_API = 'dict.1.1.20170928T063748Z.57143ef7a8afe8de.f3e0a314ad794400f3f9fd7f69156b5aa8222dfd'
BASE_URL_YANDEX_API = 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key='+KEY_API+'&lang=ru-ru&text='


def GetSynonymsDictByYandexAPI(Word):
	Response = requests.post(BASE_URL_YANDEX_API+Word)
	print (' 	 Поиск синонимов с помощью Яндекс API')

	synonyms = []
	ResponseTXT = str(Response.text)
	result = re.split(r'[^а-я ]', ResponseTXT)
	result = [v for v in result if v]

	if len(result) == 0:
		print (' 	  Синонимы не найдены')
		return []
	else:
		synonyms = result
	print (' 	  done')
	return synonyms


def SearchSynonyms(Word):
	print (' 	 Поиск синонимов на сайте: ' + BASE_URL[:-26])
	morph  = pymorphy2.MorphAnalyzer()

	Payload = {'word': Word}
	Response = requests.post(BASE_URL, 
							 data=Payload)

	synonyms = []
	synonyms.clear()

	try:
		synonyms = eval(Response.text)['synonyms']
	except KeyError:
		print (' 	  Синонимы не найдены')
	print (' 	  done')

	synonyms.extend(GetSynonymsDictByYandexAPI(Word))	
	synonyms.extend(NewSynonymsFromSite.main(Word))
	synonyms.extend(NewSynonymsFromSite1.main(Word))
	synonyms = list(set(synonyms))
	
	counter = 0
	while counter < len(synonyms):
		ParticleWord = morph.parse(synonyms[counter])[0].tag.POS
		if synonyms[counter].count(' ') > 0 or (ParticleWord != 'NOUN' and ParticleWord != 'ADJF'):
			synonyms.remove(synonyms[counter])
			synonyms.insert(counter, '')

		counter += 1

	synonyms = [x for x in synonyms if x != '']
	return synonyms


def GetSynonyms(param, Word):
	SynonymsDict = {}
	SynonymsDict[param] = Word
	SynonymsDict['Synonyms'] = SearchSynonyms(Word)
	return SynonymsDict



if __name__ == "__main__": 
	InputWord = sys.argv[1]
	Parameter = sys.argv[2]
	print (GetSynonyms(Parameter, InputWord))