"""
Убирает синонимы из описания фильмов 
"""
import json
import sqlite3 as lite
import datetime
import os
from itertools import groupby

DIRECTORY = '/media/sf_D_DRIVE/FILMPRJ/films'


def CheckSynonms(Sphere, ListDump):
	CounterClass = 0
	#print (ListDump)
	db_action = Config_Parameters_DB.cursor()
	while CounterClass < len(ListDump):
		Word = ListDump[CounterClass]
		db_action.execute('SELECT Tag FROM TagsSynonyms Where Synonyms=? or Tag=?', 
						  [Word, Word])
		#print (CounterClass, ListDump[CounterClass])
		Tag = str(db_action.fetchone())[2:-3]
		#print (Tag)

		db_action.execute('SELECT Synonyms FROM TagsSynonyms Where Synonyms=?', 
						  [Word])
		Synonyms = str(db_action.fetchone())[2:-3]

		#print (Synonyms)
		
		if Tag == '':
			#Добавить в отдельную таблицу это слово
			db_action.execute('SELECT Word FROM NewWords Where Word=?', 
						      [Word])
			WordINDB = str(db_action.fetchone())[2:-3]
			if len(WordINDB) == 0:
				db_action.execute("INSERT INTO NewWords VALUES(?, ?)", 
							   	  [Word, Sphere])

			ListDump.remove(Word)
			ListDump.insert(CounterClass, '')
		else:
			if Synonyms != '':
				if ListDump.count(Tag) == 0:
					#print (Tag)
					#print (ListDump)
					ListDump.remove(Word)
					ListDump.insert(CounterClass, Tag)
				else:
					ListDump.remove(Word)
					ListDump.insert(CounterClass, '')


		
		CounterClass += 1
	
	db_action.close()

	while '' in ListDump:
		ListDump.remove('')

	return list(set(ListDump))
	#print (ListDump)



def ProcClass(ClassDump, Sphere):
	ClassCounter = 0
	while ClassCounter < len(ClassDump):
		Dict = ClassDump[ClassCounter]

		if len(Dict['features']) != 0:
			FeatList = Dict['features']
			ClassDump[ClassCounter]['features'] = CheckSynonms(Sphere, FeatList)

		if Dict.get('specials') != None:
			SpecialsList = Dict['specials']
			ClassDump[ClassCounter]['specials'] = CheckSynonms(Sphere, SpecialsList)
		
		ClassCounter += 1
	#print (ClassDump)
	return ClassDump



def start():
	"""
	Обработка описаний фильмов
	"""
	FilmsCounter = 0
	FilmsNamesList = os.listdir(DIRECTORY)
	while FilmsCounter < len(FilmsNamesList):
		"""
		Открытие n-го фильма и получение данных
		"""
		with open('films/'+str(FilmsNamesList[FilmsCounter]), 
				  'r', encoding='utf-8') as file:
		
			Dump = json.load(file)
			file.close()

		print ('Обработка фильма №%d - %s' %(FilmsCounter, Dump['name']))

		now = datetime.datetime.now()
		Dump['__updated'] = str(now.day) + '.' +\
							str(now.month) + '.' +\
							str(now.year)
		"""
		Обработка каждой сферы (проверка на синонимичность)
		"""
		Dump['stories'] = ProcClass(Dump['stories'], 'stories')
		Dump['heroes'] = ProcClass(Dump['heroes'], 'heroes')
		Dump['places'] = ProcClass(Dump['places'], 'places')
		Dump['times'] = ProcClass(Dump['times'], 'times')
		Dump['atmospheres'] = ProcClass(Dump['atmospheres'], 'atmospheres')
		Dump['problems'] = ProcClass(Dump['problems'], 'problems')
		"""
		Сохранение n-го описания фильма в файл
		"""
		with open('films/'+str(FilmsNamesList[FilmsCounter]), 
				  'w', encoding='utf-8') as file:
	
			json.dump(Dump, file, sort_keys=True, 
					  indent=4, ensure_ascii=False)
			file.close()

		FilmsCounter += 1

if __name__ == "__main__": 
	Config_Parameters_DB = lite.connect('Synonyms.db')
	start()
	Config_Parameters_DB.commit()