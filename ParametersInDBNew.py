# -*- coding: utf8 -*-

import requests
from bs4 import BeautifulSoup
import sqlite3 as lite
import re
import json
import pymorphy2
import GetSynonyms
import os
import hashlib


class WordGroupsHandler():
	'''
	1. Получение словаря слов вида:
			{'sphere':n, 'words':[w1, ..., wm]}
	2. К каждому слову из 'words' ищутся синонимы
	'''
	def __init__(self):
		super().__init__()
		self.MainMethod()


	def MainMethod(self):
		WordsDict = self.LoadFilesInDict()
		WordsDict = self.WordsDictHandler(WordsDict)

	"""
	Получение словаря из файлов
	"""
	def LoadFilesInDict(self):
		TagsFromSiteList = []
		FilesInDirectory = os.listdir()
		for i in range(len(FilesInDirectory)):
			File = FilesInDirectory[i]
			if File.find('words_') != -1:
				TagsFromSiteList.append(File)
	
		TagsFromSiteDict = {}
		for i in range(len(TagsFromSiteList)):
			File = TagsFromSiteList[i]
			TagsFromSiteDict[i+1] = {}
			TagsFromSiteDict[i+1]['sphere_name'] = File[6:-4]

			with open(File, 'r') as f:
				Words = f.readlines()
				Words = list(set(Words))
				Words = self.WordsPrettyfy(Words)
				TagsFromSiteDict[i+1]['words'] = Words

		return TagsFromSiteDict

	"""
	Удаление переноса строк
	"""
	def WordsPrettyfy(self, Words):
		for WordNumber in range(len(Words)):
			Word = Words[WordNumber]
			Words[WordNumber] = Word.rstrip()
		return Words

	"""
	Обработка словаря слов:
		0. Поиск синонимов к словам
		1. Убрать все синонимичные слова
		2. Обработать слова в сферах:
			2.1. Основная идея сюжета - только сущиствительные
			2.2. Атмосфера фильма - только слова на "ая"
			2.3. Время событий - забить жестко тем, что есть на сайте
			2.4. Место - оставить как есть
			2.5. Особенности одного из ключевых героев - сущ. и прил. (ый ой)
			2.6. Особенности сюжета - существительные
	"""
	def WordsDictHandler(self, WordsDict):
		AllWords = []#Для проверки на синонимичные слова
		BadWords = []
		for SphereNumber in range(1, len(WordsDict)+1):
			SphereName = WordsDict[SphereNumber]['sphere_name']
			SphereWords = WordsDict[SphereNumber]['words']
			"""
			Получение групп слов с идентификатором и
			Списка всех добавленных слов
			"""
			print (SphereWords)
			WordsDict[SphereNumber]['words'], AllWords, NewBadWords = self.SpheresHandle(SphereName, SphereWords, AllWords)
			BadWords.extend(NewBadWords.copy())
		self.SaveBadWordsInFile(BadWords)
		self.SaveSynonymsInFile(WordsDict)
		self.SaveSynonymsInDB(WordsDict)

	def SpheresHandle(self, SphereName, SphereWords, AllWords):
		print ('Searching words for sphere '+SphereName)
		SphereWordsGroupDict = {}#1 group
		SphereWordGroupsList = []#all groups
		BadWords = []

		morph  = pymorphy2.MorphAnalyzer()

		for WordNumber in range(len(SphereWords)):
			Word = SphereWords[WordNumber]
			print (' Word - '+Word)
			print ('  Checking word...')
			if AllWords.count(Word) != 0:
				print ("   That's not a new word!")
				BadWords.append(Word)
			else:
				print ("   done")
				print ('  Serching sinonyms...')
				"""
				Получение списка сырых синонимов
				"""
				WordsGroupList = GetSynonyms.GetSynonyms(Word)
				"""
				Получение списка синонимов, обработанных в соответствии
				со сферой
				"""
				WordsGroupList = self.ChooseSphereHandler(SphereName, 
														  WordsGroupList, 
														  morph)
				"""
				Получение списка синонимов, исключающих используемые ранее слова
				"""
				WordsGroupList, AllWords = self.CheckingOnSynonyms(WordsGroupList, 
																   AllWords)
				if len(WordsGroupList) == 0:
					BadWords.append(Word)

				GroupID = hashlib.sha256(Word.encode()).hexdigest()
				SphereWordsGroupDict['group_id'] = GroupID

				SphereWordsGroupDict['group'] = []
				SphereWordsGroupDict['group'].extend(WordsGroupList)
				WordsGroupList.clear()

				SphereWordGroupsList.append(SphereWordsGroupDict.copy())
				SphereWordsGroupDict.clear()

		return SphereWordGroupsList, AllWords, BadWords


	def ChooseSphereHandler(self, SphereName, WordsGroupList, morph):
		if SphereName == 'story':
			return self.ProblemAndStorySphereWordsHandler(WordsGroupList, 
								         	    morph)
			
		if SphereName == 'time':
			return WordsGroupList

		if SphereName == 'problem':
			return self.ProblemAndStorySphereWordsHandler(WordsGroupList, 
										          morph)

		if SphereName == 'place':
			return WordsGroupList

		if SphereName == 'hero':
			return self.HeroSphereWordsHandler(WordsGroupList, 
											   morph)
		
		if SphereName == 'atmosphere':
			return self.AtmosphereSphereWordsHandler(WordsGroupList, 
													 morph)



	def AtmosphereSphereWordsHandler(self, WordsGroupList, morph):
		for WordNumber in range(len(WordsGroupList)):
			Word = WordsGroupList[WordNumber]

			ParticleWord = morph.parse(Word)[0].tag.POS
			"""
			Оставить только краткие и полные прилагательные
			"""
			if ParticleWord != 'ADJF' and ParticleWord != 'ADJS' and\
										  ParticleWord != 'NOUN' and\
			  								   Word[-2:] != 'ый' and\
			  									   Word[-2:] != 'ой':
				WordsGroupList.remove(Word)
				WordsGroupList.insert(WordNumber, '')

		WordsGroupList = [x for x in WordsGroupList if x != '']
		return WordsGroupList


	def ProblemAndStorySphereWordsHandler(self, WordsGroupList, morph):
		for WordNumber in range(len(WordsGroupList)):
			Word = WordsGroupList[WordNumber]

			ParticleWord = morph.parse(Word)[0].tag.POS
			"""
			Оставить только краткие и полные прилагательные
			"""
			if ParticleWord != 'NOUN':
				WordsGroupList.remove(Word)
				WordsGroupList.insert(WordNumber, '')

		WordsGroupList = [x for x in WordsGroupList if x != '']
		return WordsGroupList


	def HeroSphereWordsHandler(self, WordsGroupList, morph):
		for WordNumber in range(len(WordsGroupList)):
			Word = WordsGroupList[WordNumber]

			ParticleWord = morph.parse(Word)[0].tag.POS
			"""
			Оставить только краткие и полные прилагательные
			"""
			if ParticleWord != 'ADJF' and ParticleWord != 'ADJS' and\
			  									   Word[-2:] != 'ая':
				WordsGroupList.remove(Word)
				WordsGroupList.insert(WordNumber, '')

		WordsGroupList = [x for x in WordsGroupList if x != '']
		return WordsGroupList


	"""
	Проверка слов из WordsGroupList на встречаемость в
	AllWords (убирает уже встречающиеся слова)
	"""
	def CheckingOnSynonyms(self, WordsGroupList, AllWords):
		for WordNumber in range(len(WordsGroupList)):
			Word = WordsGroupList[WordNumber]
			if AllWords.count(Word) != 0:
				WordsGroupList.remove(Word)
				WordsGroupList.insert(WordNumber, '')

		WordsGroupList = [x for x in WordsGroupList if x != '']
		AllWords.extend(WordsGroupList)

		return WordsGroupList, AllWords


	def SaveBadWordsInFile(self, BadWords):
		with open('BadWords.txt', 'w', encoding='utf-8') as file:
			file.write(', '.join(BadWords))


	def SaveSynonymsInFile(self, Dictionary):
		with open('SynonymsTags.json', 'w', encoding='utf-8') as file:
			json.dump(Dictionary, file, 
				 	  sort_keys=True, 
				  	  indent=4, 
				  	  ensure_ascii=False)


	def SaveSynonymsInDB(self, SynonymsDict):
		Counter = 1
		Config_Parameters_DB = lite.connect('Synonyms.db')
		for SphereNumber in range(1, len(SynonymsDict)+1):
			SphereGroupsList = SynonymsDict[SphereNumber]['words'].copy()
			for GroupNumber in range(len(SphereGroupsList)):
				GroupDict = SphereGroupsList[GroupNumber].copy()
				GroupID = GroupDict['group_id']
				WordsList = GroupDict['group'].copy()
				for WordNumber in range(len(WordsList)):
					Word = WordsList[WordNumber]
					db_action = Config_Parameters_DB.cursor()
					db_action.execute("INSERT INTO Groups VALUES(?, ?)", 
								   	  [GroupID, Word])
		Config_Parameters_DB.commit()
		db_action.close()


if __name__ == '__main__':
    
    start = WordGroupsHandler()
